﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace dips
{
	[TestClass]
	public class ToDoTest
	{
		ToDoList todo;

		[TestInitialize]
		public void TestInitialize()
		{
			todo = new ToDoList();
			todo.initData();
		}


		[TestMethod]
		public void add1TodoTest()
		{
			todo.addItem("apply to dips");
			Assert.AreEqual("apply to dips", todo.list[0]);
		}

		[TestMethod]
		public void add2TodosTest()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual("get job", todo.list[1]);
		}

		[TestMethod]
		public void getItemValidIndexTest()
		{
			todo.addItem("Apply to dips");
			Assert.AreEqual("Apply to dips", todo.getItem(1));
		}
		[TestMethod]
		public void getItemIndexOutOFBoundLowerTest()
		{
			todo.addItem("Apply to dips");
			Assert.AreEqual("no task", todo.getItem(0));
		}
		[TestMethod]
		public void getItemIndexOutOfBoundHighTest()
		{
			todo.addItem("Apply to dips");
			Assert.AreEqual("no task", todo.getItem(2));
		}


		[TestMethod]
		public void do2ValidIndexTest()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual("get job", todo.doItem(2));
		}

		[TestMethod]
		public void do0IndexOutOfBoundLowerTest()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual("no task", todo.doItem(0));
		}
		[TestMethod]
		public void do3IndexOutOfBoundHigherTest()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual("no task", todo.doItem(3));
		}

		[TestMethod]
		public void do9000IndexOutOfRange()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual("no task", todo.doItem(9000));
		}

		[TestMethod]
		public void printListTest()
		{
			todo.addItem("Apply to dips");
			todo.addItem("get job");
			Assert.AreEqual(2, todo.printList());
		}

		[TestMethod]
		public void writeXMLTest()
		{
			Assert.IsTrue(todo.addToXML("test123"));
			Assert.IsTrue(todo.removeFromXML(0));
		}

		[TestCleanup]
		public void cleanXML()
		{
			try
			{
				if (File.Exists(@"todo.xml"))
				{
					File.Delete(@"todo.xml");
				}
			} catch (Exception e)
			{
				Assert.Fail(e.Message);
			}
			
		}
	}
}