using System;
using System.Collections;
using System.IO;
using System.Data;

namespace dips
{
    public class ToDoList
    {
		public ArrayList list = new ArrayList();
		DataSet d = new DataSet();
		DataTable t = new DataTable();

		public static void Main(string[] args)
        {
			string input;
			string command;
			bool exit = false;
			ToDoList todo = new ToDoList();

			//Welcome
			Console.Title = ("Schjems' Todo-List application");
            Console.WriteLine("This is a Todo-List application");
			Console.WriteLine("Type help to get a list of available commands\n");

			//Initialize data
			if (todo.initData())
			{
				Console.WriteLine("Loading your Todo-list from todo.xml..");
				todo.printList();
			}

			
			while (!exit)
			{
				Console.Write(">");
				input = Console.ReadLine();
				if (input.Length <= 0) continue;
				command = input.Split(' ')[0].ToLower();
				input = input.Remove(0, command.Length);

				switch (command)
				{
					case "add":
						input = input.Remove(0, 1); //removes space
						todo.addItem(input);
						Console.Clear();
						todo.printList();
						break;
					case "do":
						int index;
						int.TryParse(input, out index);
						if (index <= 0 || index > todo.list.Count)
						{
							Console.WriteLine("Invalid index inserted after 'do'");
							break;
						}
						Console.WriteLine("Task" + input + ": '" + todo.doItem(index) + "', has been marked as done");
						Console.ReadKey();
						Console.Clear();
						todo.printList();
						break;
					case "help":
					case "man":
						Console.Clear();
						todo.printList();
						Console.WriteLine("\nList of available commands:");
						Console.WriteLine("add <text>	| Adds task to Todo-list			| Example: add buy milk");
						Console.WriteLine("do <index>	| Marks task as done and removes it from list	| Example: do 3");
						Console.WriteLine("print		| Shows todo-list				| Alternatives: list or show");
						Console.WriteLine("exit		| Exits application				| Alternatives: quit or stop\n");

						break;
					case "print":
					case "show":
					case "list":	
						Console.Clear();
						todo.printList();
						break;
					case "exit":
					case "quit":
					case "stop":
						exit = true;
						break;
					default:
						Console.WriteLine(command + " is not a valid command. Type help for available commands");
					break;
				}
			}
            
        }

		public int printList()
		{
			int i = 0;
			for (i = 0; i < list.Count; i++)
			{
				Console.WriteLine(" {0,-4}{1}",(i + 1)+")",list[i]);
			}
			return i;
		}

		public string doItem(int index)
		{
			if (index > list.Count || index <= 0) return "no task";
			string temp;
			temp = "" + list[index-1];
			list.RemoveAt(index-1);
			if (!removeFromXML(index - 1)) Console.WriteLine("Writing to xml failed");
			return temp;
		}

		public int addItem(string input)
		{
			list.Add(input);
			if (!addToXML(input)) Console.WriteLine("Writing to xml failed");
			return list.Count-1;
		}
		public string getItem(int index)
		{
			if (index > list.Count || index <= 0) return "no task";
			return (string)list[index-1];
		}

		public bool addToXML(string task)
		{
			try
			{
				t.Rows.Add(task);
				d.WriteXml("todo.xml");
				return true;
			} catch (Exception)
			{
				return false;
			}
		}
		public bool removeFromXML(int index)
		{
			try
			{
				t.Rows.RemoveAt(index);
				d.WriteXml("todo.xml");
				return true;
			} catch (Exception)
			{
				return false;
			}
		}
		public bool initData()
        {
			d.Tables.Add(t);
			t.Columns.Add(new DataColumn("ToDos", typeof(string)));
			try
			{
				if (File.Exists("todo.xml"))
				{
					d.ReadXml("todo.xml");
					foreach (DataRow row in d.Tables[0].Rows)
					{
						list.Add((string)row["ToDos"]);
					}
					return true;

				}
				else
				{
					return false;
				}
			} catch (Exception e)
			{
				Console.WriteLine("Error:"+e.Message);
				return false;
			}
			
		}
	}
}
